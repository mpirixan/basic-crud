import http from "../http-common";

class TutorialDataService {
  getAll() {
    return http.get("/accounts");
  }

  get(id) {
    return http.get(`/accounts/${id}`);
  }

  create(data) {
    return http.post("/accounts", data);
  }

  update(id) {
    return http.put(`/accounts/update/${id}`);
  }

  delete(id) {
    return http.delete(`/accounts/${id}`);
  }

  updateBalance(id) {
    return http.get(`/accounts/balance/${id}`);
  }
}

export default new TutorialDataService();
