package com.basa.crud.entities;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity //@IdClass(Account.class)
@Table(name = "account")
public class Account implements Serializable{


	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "name", updatable = false)
	private String name;
	
	@Column(name = "email",unique = true, updatable = false)
	private String email;

	@Column(name = "password", updatable = false)
	 private String password;

	@Column(name = "balance", updatable = true)
	private Long balance;
	

	public Account() {
		
	}

	public Account(String name, String email, Long id, String password, Long balance) {
		super();
		this.name = name;
		this.email = email;
		this.id = id;
		this.password = password;
		this.balance = balance;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getBalance() {
		return balance;
	}

	public void setBalance(Long balance) {
		this.balance = balance;
	}
	
	
	@Override
	public String toString() {
		return "Account [id=" + id + ", name=" + name + ", email=" + email + ", balance=" + balance + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Account account = (Account) o;
		return id.equals(account.id) && email.equals(account.email);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, email);
	}
	
}
