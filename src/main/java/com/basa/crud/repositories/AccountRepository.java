package com.basa.crud.repositories;

//import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.basa.crud.entities.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {

	//List<Account> findByAccountContaing(String name);
}
