package com.basa.crud.services;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.basa.crud.entities.Account;
import com.basa.crud.repositories.AccountRepository;
import com.basa.crud.services.exceptions.DatabaseException;
import com.basa.crud.services.exceptions.ResourceNotFoundException;

@Service
public class AccountService {

	
	@Autowired
	private AccountRepository repository;
	
	public List<Account> findAll(){
		return repository.findAll();
	}
	
	public Account findById(Long id) {
		Optional<Account> obj = repository.findById(id);
		return obj.orElseThrow(() -> new ResourceNotFoundException(id));
	}
	
	public Account insert(Account obj) {
		//TODO
		/* TRATAR org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException*/
		return repository.save(obj);
	}
	
	public void delete(Long id) {
		try {
		repository.deleteById(id);
		}catch (EmptyResultDataAccessException e) {
			throw new ResourceNotFoundException (id);
			
			}catch (DataIntegrityViolationException e) {
				throw new DatabaseException(e.getMessage());
			}
		}
	
	
	public Account update(Long id, Account obj) { 
		try {
		@SuppressWarnings("deprecation")
		Account entity = repository.getOne(id);
		updateData(entity, obj);
		updateBalance(entity, obj);
		return repository.save(entity);
		}catch (EntityNotFoundException e) {
			throw new ResourceNotFoundException(id);
		}
	}

	private void updateData(Account entity, Account obj) {
		entity.setName(obj.getName());
		entity.setEmail(obj.getEmail());
		
		
	}
	
	private void updateBalance(Account entity, Account obj) {
		entity.setBalance(obj.getBalance());

	}
	
}
